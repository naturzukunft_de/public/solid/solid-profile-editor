package de.naturzukunft.solid;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.stream.Collectors;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.solid.oauth2.DPopProvider;
import ezvcard.VCard;
import ezvcard.property.Photo;
import reactor.core.publisher.Mono;

@Controller
public class MainController {

	private WebClient.Builder wcBuilder;
	private WebClient webClient;
	private DPopProvider dPopProvider;

	public MainController(@LoadBalanced WebClient.Builder wcBuilder, DPopProvider dPopProvider, WebClient webClient) {
		this.wcBuilder = wcBuilder;
		this.dPopProvider = dPopProvider;
		this.webClient = webClient;
	}

	@GetMapping("/")
	public String index(Model model, @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
			@AuthenticationPrincipal OAuth2User oauth2User) {
		model.addAttribute("userName", oauth2User.getName());
		model.addAttribute("clientName", authorizedClient.getClientRegistration().getClientName());
		model.addAttribute("userAttributes", oauth2User.getAttributes());
		return "index";
	}

	@GetMapping("/userprofile")
	public String userprofile(Model model, @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
			@AuthenticationPrincipal OAuth2User oauth2User) {

		String username = authorizedClient.getClientRegistration().getClientId();

		String podResponse = getRequest(oauth2User.getName(),
				"DPoP " + authorizedClient.getAccessToken().getTokenValue(), username);
		System.out.println(podResponse);
		org.apache.jena.rdf.model.Model pod = toJenaModel(podResponse, oauth2User.getName());

		Resource me = pod.getResource(oauth2User.getName());
		ResourceHandler rh = new ResourceHandler();
		rh.handle(0, me);

		VCardConverter vCardConverter = new VCardConverter();
		VCard vCard = vCardConverter.convert(me);
		java.util.Map<String, Object> attributes = toAttributesMap(vCard);

		model.addAttribute("userAttributes", attributes);
		for (Photo photo : vCard.getPhotos()) {
			int i = 1;
			model.addAttribute("pic", photo.getUrl());
			// TODO View anpassen für mehrere Bilder
		}

		return "userprofileView";
	}

	private String getRequest(String uri, String authorizationHeader, String userName) {
		Mono<String> podMono = webClient.get().uri(uri).header("Authorization", authorizationHeader)
				.header("DPoP", dPopProvider.getDPopToken(userName, HttpMethod.GET, URI.create(uri))).retrieve()
				.bodyToMono(String.class);

		return podMono.block();
	}

	private static org.apache.jena.rdf.model.Model toJenaModel(String data, String baseUrl) {
		org.apache.jena.rdf.model.Model model = ModelFactory.createDefaultModel();
		InputStream in = new ByteArrayInputStream(data.getBytes());
		model.read(in, baseUrl, "TTL");
		return model;
	}

	@GetMapping("/inbox")
	public String inbox(Model model, @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
			@AuthenticationPrincipal OAuth2User oauth2User) {

		String username = authorizedClient.getClientRegistration().getClientId();
		
		String podResponse = getRequest("https://naturzukunft.solidcommunity.net/inbox/",
				"DPoP " + authorizedClient.getAccessToken().getTokenValue(), username);
		System.out.println(podResponse);
		org.apache.jena.rdf.model.Model pod = toJenaModel(podResponse, oauth2User.getName());

		return "inbox";
	}

	private java.util.Map<String, Object> toAttributesMap(VCard vcard) {
		java.util.Map<String, Object> attributes = new java.util.HashMap<String, Object>();

		attributes.put("Name", vcard.getFormattedName().getValue());
		attributes.put("Role", vcard.getRoles().stream().map(card -> card.getValue()).collect(Collectors.joining(",")));
		attributes.put("Organisation",
				vcard.getOrganization().getValues().stream().map(Object::toString).collect(Collectors.joining(",")));

		for (int i = 0; i < vcard.getAddresses().size(); i++) {
			attributes.put(i + ". City", vcard.getAddresses().get(i).getLocality());
			attributes.put(i + ". Country", vcard.getAddresses().get(i).getCountry());
			attributes.put(i + ". Region", vcard.getAddresses().get(i).getRegion());
		}

		return attributes;
	}
}