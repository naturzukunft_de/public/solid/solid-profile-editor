package de.naturzukunft.solid;

import java.net.URI;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.ClientResponse.Headers;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.solid.oauth2.DPopProvider;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Controller
@Slf4j
public class WriteController {

	private WebClient.Builder wcBuilder;
	private DPopProvider dPopProvider;
	
	public WriteController( @LoadBalanced WebClient.Builder wcBuilder, DPopProvider dPopProvider) {
		this.wcBuilder = wcBuilder;
		this.dPopProvider = dPopProvider;
	}

	@GetMapping("/addContact")
	public String userprofile(Model model,
						@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
						@AuthenticationPrincipal OAuth2User oauth2User) {
		
		String content = "@prefix ro:     http://purl.org/wf4ever/ro#\n" + 
				"@prefix dct:    http://purl.org/dc/terms/\n" + 
				"@prefix ore:    http://www.openarchives.org/ore/\n" + 
				"\n" + 
				"<> a ro:ResearchObject, ore:Aggregation ;\n" + 
				"    dct:created \"2012-12-01\"^^xsd:dateTime .";

		reactor.netty.http.client.HttpClient httpClient = HttpClient
		          .create()
//		          .tcpConfiguration(
//		            tc -> tc.bootstrap(
//		              b -> BootstrapHandlers.updateLogSupport(b, new CustomLogger(HttpClient.class))))
		          ;
		
		System.out.println(authorizedClient.getAccessToken().getClass().getName());
		System.out.println(authorizedClient.getAccessToken().getTokenValue());
		
//		Jwt jwt = Jwt.withTokenValue(authorizedClient.getAccessToken().getTokenValue()).build();
		
//		System.out.println("Audience: " + jwt.getAudience());
//		jwt.getClaims().forEach((name, values) -> log.debug( name +"-"+ values));
//		jwt.getHeaders().forEach((name, values) -> log.debug( name +"-"+ values));
		
		
		
		String url = getBaseUrl(authorizedClient) + "/";
		Mono<String> podMono = wcBuilder
				.filters(exchangeFilterFunctions -> {
				      exchangeFilterFunctions.add(logRequest());
				      exchangeFilterFunctions.add(logResponse());
				  })
				.clientConnector(new ReactorClientHttpConnector(httpClient))
				.build()
				.post().uri(URI.create(url).toString())				
				.header("Authorization", "DPoP " + authorizedClient.getAccessToken().getTokenValue())
				.header("Link", "<http://www.w3.org/ns/ldp#BasicContainer>; rel=\"type\"")
				.header("Slug", "data")				
				.header("DPoP", dPopProvider.getDPopToken(authorizedClient.getClientRegistration().getClientId(), HttpMethod.POST, URI.create(url)))
				.bodyValue(content)
				.retrieve()
				.bodyToMono(String.class);
		
		System.out.println(podMono.block());
		
		return "index";
	}
	
	private String getBaseUrl(OAuth2AuthorizedClient authorizedClient) {
		if("inrupt".equals(authorizedClient.getClientRegistration().getClientName())){
	  		return "https://naturzukunft.inrupt.net"; 
		} else if("solidcommunity".equals(authorizedClient.getClientRegistration().getClientName())){
			return "https://naturzukunft.solidcommunity.net";
		} else {
			throw new RuntimeException();
		}
	}

	private ExchangeFilterFunction logRequest() {
	    return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Request: \n");
	            sb.append(clientRequest.method()).append("\n");
	            sb.append(clientRequest.url()).append("\n");
	            clientRequest
	              .headers()
	              .forEach((name, values) -> {
	            	  sb.append("Header: " + name + " - " + values).append("\n");
	              }
	    		);
	            log.debug(sb.toString());
	        }
	        return Mono.just(clientRequest);
	    });
	}
	
	private ExchangeFilterFunction logResponse() {
	    return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Response: \n");
	            //append clientRequest method and url
	            Headers headers = clientResponse.headers();
	            headers.asHttpHeaders().forEach((name, values) -> values.forEach(value -> log.debug( "-"+ value)));
	            log.debug(sb.toString());
	        }
	        return Mono.just(clientResponse);
	    });
	}
	
}
