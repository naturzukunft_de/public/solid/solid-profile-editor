package de.naturzukunft.solid.oauth2;

import java.net.URI;

import org.springframework.http.HttpMethod;

public interface DPopProvider {


	String getDPopToken(String userName, HttpMethod httpMethod, URI uri);
}