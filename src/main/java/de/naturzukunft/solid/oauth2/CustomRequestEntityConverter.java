package de.naturzukunft.solid.oauth2;

import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequestEntityConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class CustomRequestEntityConverter implements Converter<OAuth2AuthorizationCodeGrantRequest, RequestEntity<?>> {
	 
    private OAuth2AuthorizationCodeGrantRequestEntityConverter defaultConverter;
    private DPopProvider dPopProvider;
    
    public CustomRequestEntityConverter(DPopProvider dPopProvider) {
		this.dPopProvider = dPopProvider;
		defaultConverter = new OAuth2AuthorizationCodeGrantRequestEntityConverter();
    }
    
    @Override
    public RequestEntity<?> convert(OAuth2AuthorizationCodeGrantRequest req) {
        RequestEntity<?> entity = defaultConverter.convert(req);
        
		HttpHeaders myHeaders = getHeaders(entity);
		myHeaders.add("DPoP", dPopProvider.getDPopToken(req.getClientRegistration().getClientId(), entity.getMethod(), entity.getUrl()));
		
		@SuppressWarnings("unchecked")
		MultiValueMap<String, String> params = (MultiValueMap<String,String>) entity.getBody();
		params.put("clientId", List.of(req.getClientRegistration().getClientId()));
		params.put("clientSecret", List.of(req.getClientRegistration().getClientSecret()));
		
		
		logHeaders(myHeaders);
		logBodyParams(entity);
		
		req.getClientRegistration().getClientId();
		req.getClientRegistration().getClientSecret();
//		add clientId und Secret to params
		
		RequestEntity<?> re = new RequestEntity<>(entity.getBody(), myHeaders, 
		          entity.getMethod(), entity.getUrl());
		
        return re;
    }

	private void logBodyParams(RequestEntity<?> entity) {
		@SuppressWarnings("unchecked")
		MultiValueMap<String, String> params = (MultiValueMap<String,String>) entity.getBody();
		params.forEach((key2, value) -> {
			System.out.println(String.format("param '%s' = %s", key2, value));
	    });
	}

	private void logHeaders(HttpHeaders myHeaders) {
		myHeaders.forEach((key2, value) -> {
			System.out.println(String.format("Header '%s' = %s", key2, value));
	    });
	}

	private HttpHeaders getHeaders(RequestEntity<?> entity) {
		HttpHeaders myHeaders = new HttpHeaders();
		entity.getHeaders().forEach((key, value) -> {
			myHeaders.put(key, value);
	    });
		return myHeaders;
	}
}
