package de.naturzukunft.solid;

import java.util.Optional;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.VCARD4;

import ezvcard.VCard;
import ezvcard.VCardVersion;
import ezvcard.parameter.ImageType;
import ezvcard.property.Address;
import ezvcard.property.Photo;
import ezvcard.property.Role;

public class VCardConverter {

	public VCard convert(Resource solidProfile) {
		VCard vcard = new VCard(VCardVersion.V4_0);
		
		getLiteral(solidProfile, VCARD4.fn).ifPresent(fn -> vcard.setFormattedName(fn));
		getLiteral(solidProfile, VCARD4.role).ifPresent(role -> vcard.getRoles().add(new Role(role)));
		getLiteral(solidProfile, VCARD4.organization_name).ifPresent(orgName -> vcard.setOrganization(orgName));
		getPhoto(solidProfile).ifPresent(photo -> vcard.getPhotos().add(photo));
		
		if(solidProfile.getProperty(VCARD4.hasAddress) != null) {
			Resource address = solidProfile.getProperty(VCARD4.hasAddress).getResource();
			Address vcardAddress = new Address();
			getLiteral(address, VCARD4.locality).ifPresent(locality -> vcardAddress.setLocality(locality));
			getLiteral(address, VCARD4.country_name).ifPresent(countryName -> vcardAddress.setCountry(countryName));
			getLiteral(address, VCARD4.region).ifPresent(region -> vcardAddress.setRegion(region));
			vcard.getAddresses().add(vcardAddress);			
		}
		
		return vcard;
	}

	private Optional<Photo> getPhoto(Resource solidProfile) {
		if(solidProfile.getProperty(VCARD4.hasPhoto) != null) {
			Statement photoStatement = solidProfile.getProperty(VCARD4.hasPhoto);
			String pic = photoStatement.getResource().toString();
			String extension = pic.substring(pic.lastIndexOf(".")+1);
			switch (extension) {
				case "JPG":
				case "jpg":
					return Optional.of(new Photo(pic, ImageType.JPEG));
	
				case "PNG":
				case "png":
					return Optional.of(new Photo(pic, ImageType.PNG));
	
				case "GIF":
				case "gif":
					return Optional.of(new Photo(pic, ImageType.GIF));
			}			
		}
		return Optional.empty();
	}
	
	private Optional<String> getLiteral(Resource me, Property property) {
		Optional<String> literalOptional = Optional.empty();
		if (me.getProperty(property) != null 
			&& me.getProperty(property).getObject() != null
			&& me.getProperty(property).getObject().isLiteral()) {
				literalOptional = Optional.ofNullable(me.getProperty(property).getString());
		}
		return literalOptional;
	}
}
