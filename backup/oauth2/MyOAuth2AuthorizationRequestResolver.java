package de.naturzukunft.solid.oauth2;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

public class MyOAuth2AuthorizationRequestResolver implements OAuth2AuthorizationRequestResolver {

	private OAuth2AuthorizationRequestResolver defaultResolver;

	public MyOAuth2AuthorizationRequestResolver(ClientRegistrationRepository repo, String authorizationRequestBaseUri) {
		defaultResolver = new DefaultOAuth2AuthorizationRequestResolver(repo, authorizationRequestBaseUri);
	}

	@Override
	public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
		OAuth2AuthorizationRequest req = defaultResolver.resolve(request);
		if (req != null) {
			req = customizeAuthorizationRequest(req);
		}
		return req;
	}

	@Override
	public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String clientRegistrationId) {
		OAuth2AuthorizationRequest req = defaultResolver.resolve(request, clientRegistrationId);
		if (req != null) {
			req = customizeAuthorizationRequest(req);
		}
		return req;
	}

    @Autowired
    private JwkKeyPairManager keyPairManager;

    
	private OAuth2AuthorizationRequest customizeAuthorizationRequest(OAuth2AuthorizationRequest req) {
		
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put("public_key", keyPairManager.createJWK().toJSONString());
		return OAuth2AuthorizationRequest.from(req).additionalParameters(additionalParameters).clientId("HierKommtDieWebId?").build();
	}
}
